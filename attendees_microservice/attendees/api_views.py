from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

# from events.api_views import ConferenceListEncoder
# from events.models import Conference
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO    # ADDED ConferenceVO


# ADDED NEW ENCODER:
class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }


'''

Fix the list attendees function
We're not dealing with conferences anymore. 
We're dealing with ConferenceVO objects. 
Because of that, let's change the name of the parameter of the api_list_attendees function to reflect that.

'''

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            # ADDED THIS NEW LINE:
            conference_href = f'/api/conferences/{conference_vo_id}/'

            # CHANGED Conference to ConferenceVO and id=conference_id to import_href=conference_href
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference

            ## CHANGED Conference to ConferenceVO
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    
    '''
    Now, the code expects a "conference" key in the submitted JSON-encoded data.
    It should be the "href" value for a Conference object.
    '''


def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
