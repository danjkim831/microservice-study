from django.urls import path

from .api_views import api_list_attendees, api_show_attendee

urlpatterns = [
    # ADDED THIS LINE (THIS IS FOR POST AKA CREATING A NEW ATTENDEE)
    path("attendees/", api_list_attendees, name="api_create_attendees"),
    path(
        # UPDATED <int:conference_id> TO <int:conference_vo_id> (THIS IS FOR GET AKA SHOWING THE LIST OF ATTENDEES IN A SPECIFIC CONFERENCE)
        "conferences/<int:conference_vo_id>/attendees/",
        api_list_attendees,
        name="api_list_attendees",
    ),
    path("attendees/<int:pk>/", api_show_attendee, name="api_show_attendee"),
]
