import json
import requests

from .models import ConferenceVO


# WRITING THE SYNC SCRIPT:
def get_conferences():
    # monolith:8000/api/conferences is the OTHER service in monolith
    # Runs once a minute, every minute it will fetch all the stuff from the other service (the entire list of Conferences)
    url = "http://monolith:8000/api/conferences/"
    # it's doing a GET request to this url on the other service in monolith
    response = requests.get(url)
    content = json.loads(response.content) # I think content = response.json() would work too (test it out once you've got everything down)

    # We can put some optional logging here so we can see the syncing on the other side
    # (in this specific case: the attendees-microservice)
    # because the attendees-microservice is fetching from monolith 
    print("Syncing Conferences:", content)

    for conference in content["conferences"]:
        # update_or_create: import_href is like our key and we're going to look for one that we already have (conference['href']),
        # if we already have it, it will update it with the new "name"
        # else (if it doesn't exist), it will create it with the "name"
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )


'''

update_or_create example:

obj, created = Person.objects.update_or_create(
    first_name='John', last_name='Lennon',
    defaults={'first_name': 'Bob'},
)
# If person exists with first_name='John' & last_name='Lennon' then update first_name='Bob'
# Else create new person with first_name='Bob' & last_name='Lennon'

'''


## random sidenote: Microservices are mostly applicable to large companies that have lots of code and customers
## smaller companies don't really use/need microservices